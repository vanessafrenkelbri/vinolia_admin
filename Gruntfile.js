module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),



        pug: {
            compile: {
              options: {
                data: {
                  debug: false
                },
                pretty: true
              },
              files: {
                'login.html': ['_dev/pug/login.pug'],
                'index.html': ['_dev/pug/index.pug'],
                'event-calendar.html': ['_dev/pug/event-calendar.pug'],    
                'event-new.html': ['_dev/pug/event-new.pug'],    
                'promotions-code.html': ['_dev/pug/promotions-code.pug'],
                'event-information.html': ['_dev/pug/event-information.pug'],
                'event-new-attendant.html': ['_dev/pug/event-new-attendant.pug']  




              }
            }
          },        

        sass: {
            dist: {
                options: {
                    //style: 'compressed'
                    style: 'expanded'
                },
                files: {
                    // Nuestro Sass es compilado a nuestro archivo CSS
                    'assets/css/style.css': 'assets/scss/style.scss',
                    'assets/css/style-vinolia.css': 'assets/scss/style-vinolia.scss'
                    
                }
            }
        },


        watch: {
            site: {
                // Vigilamos cualquier cambio en nuestros archivos
                files: ['assets/scss/**/*.scss', 'assets/css/*.css', 'assets/js/**/*.js', '*.html' ,'assets/js/**/*.js', '_dev/pug/**/*.pug'],
                tasks: ['default']
            },
            options: {
                // Instalamos la extensión de Livereload en Chrome para ver cambios
                // automáticos en el navegador sin hacer refresh
                spawn: false,
                livereload: true
            }
        }

    });

    // Cargamos los plugins

    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Registrar tareas
    //grunt.registerTask('default', ['pug', 'sass', 'watch']);
    //grunt.registerTask('default', ['concat', 'uglify','pug', 'sass', 'watch']);
    //grunt.registerTask('default', ['concat', 'uglify','pug', 'sass', 'scp', 'watch']);
    grunt.registerTask('default', ['pug', 'sass', 'watch']);    
    //grunt.registerTask('default', [ 'scp' ]);
    
    
}