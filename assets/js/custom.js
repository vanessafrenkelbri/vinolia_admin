jQuery(document).ready(function( $ ) {
    $(function() {
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
        $('#datetimepicker4').datetimepicker({
            format: 'LT'
        });
        $('#datetimepicker5').datetimepicker({
            format: 'L'
        });
    });
    // $('#datatable-events').DataTable();
    $(function() {

        var start = moment().subtract(29, 'days');
        var end = moment();
    
        function cb(start, end) {
            $('#eventCalendar span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    
        $('.daterange-calendar-full').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
               'Hoy': [moment(), moment()],
               'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
               'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
               'Este mes': [moment().startOf('month'), moment().endOf('month')],
               'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            "startDate": "2016-01-01",
            "endDate": "2016-01-07",
            "opens": "center"
        
        
        }, cb);
    
        cb(start, end);
    
    });

    $('#selectDates').on('click', function () {
        if ($(this).prop('checked')) {
            $('#selectDateCalendar').show();
        } else {
            $('#selectDateCalendar').hide();
        } 
    });

    $('#switch3').on('click', function(){
        if ($(this).prop('checked')) {
            $('#week-days').css({
                display:'flex',
            });
            $('#only-day').hide();

        } else {
            $('#week-days').hide();
            $('#only-day').show();

        }
    })


    $('#allweek').on('click', function(){
        if ($(this).prop('checked')) {
            $('#monday').prop( "checked", true);
            $('#tuesday').prop( "checked", true);
            $('#wednesday').prop( "checked", true);
            $('#thursday').prop( "checked", true);
            $('#friday').prop( "checked", true);
            $('#saturday').prop( "checked", true);
        } else {
            $('#monday').prop( "checked", false);
            $('#tuesday').prop( "checked", false);
            $('#wednesday').prop( "checked", false);
            $('#thursday').prop( "checked", false);
            $('#friday').prop( "checked", false);
            $('#saturday').prop( "checked", false);
        }
    })

    $("#fileDrop").dropzone({ url: "#" });

    $('#como').bind('change', function(){
        var selectVal = $(this).val();
        console.log(selectVal);
        if ( selectVal == 1 || selectVal == 2 || selectVal == 6 || selectVal == 3 || selectVal == 5) {
            $('#CNC_input').show();
        } else {
            $('#CNC_input').hide();
        }
        if ( selectVal == 4 ) {
            $('#CNC_input').hide();
        }

  });

    $('#check_Menores').on('click', function () {
        if ($(this).prop('checked')) {
            $('#how-many-kids').show();
        } else {
            $('#how-many-kids').hide();
        }
    });

    // $('.cancel').on('click', function() {
    //     window.history.back();

    // });

});
